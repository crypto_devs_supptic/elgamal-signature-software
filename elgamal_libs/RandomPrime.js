const randomPrime = require('random-prime').randomPrime;

class RandomPrimeGenerator {
    constructor() {

    }
    generateRandomPrime() {
        return (randomPrime())
    }

}
module.exports = {
    RandomPrimeGenerator
}